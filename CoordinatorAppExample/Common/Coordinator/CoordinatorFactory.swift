//
//  CoordinatorFactory.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 07.06.2021.
//

import Foundation
import UIKit

protocol CoordinatorsFactory {
    
    func makeLoginCoordinator(navController: UINavigationController) -> Coordinator & LoginCoordinatorOutput
    func makeItemsCoordinator(navController: UINavigationController) -> Coordinator & ItemsCoordinatorOutput
}

final class CoordinatorsFactoryImpl: CoordinatorsFactory {
    
    func makeLoginCoordinator(navController: UINavigationController) -> Coordinator & LoginCoordinatorOutput {
        LoginCoordinator(factory: LoginModuleFactoryImpl(), router: router(navController))
    }
    
    func makeItemsCoordinator(navController: UINavigationController) -> Coordinator & ItemsCoordinatorOutput {
        ItemsCoordinator(
            router: router(navController),
            factory: ItemsModuleFactoryImpl(),
            personalTokenStorage: AppAssembly.personalTokenService)
    }
}

private extension CoordinatorsFactoryImpl {
    func router(_ navController: UINavigationController) -> Router {
        return RouterImpl(rootController: navController)
    }
}
