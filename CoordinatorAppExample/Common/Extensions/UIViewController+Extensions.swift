//
//  UIViewController+Extensions.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 07.06.2021.
//

import Foundation
import UIKit

protocol StoryboardFactory {
    func make() -> UIViewController
}
