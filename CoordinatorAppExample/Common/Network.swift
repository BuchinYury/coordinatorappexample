//
//  Network.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 10.06.2021.
//

import Foundation

protocol Request {
    var urlRequest: URLRequest? { get }
}

enum ParsingError: Error {
    case unknown
}

protocol Parser {
    associatedtype Model
    func parse(data: Data) -> Model?
}

struct RequestDefinition<ResponseParser> where ResponseParser: Parser {
    let request: Request
    let parser: ResponseParser
}

protocol RequestSender {
    func send<ResponseParser>(
        config: RequestDefinition<ResponseParser>,
        completion: @escaping (Result<ResponseParser.Model, Error>) -> Void)
}

enum NetworkError: Error {
    case badUrlRequest
}

final class NetworkClient: RequestSender {
    
    func send<ResponseParser>(
        config: RequestDefinition<ResponseParser>,
        completion: @escaping (Result<ResponseParser.Model, Error>) -> Void) where ResponseParser : Parser {
        
        guard let urlRequest = config.request.urlRequest
        else {
            completion(.failure(NetworkError.badUrlRequest))
            return
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                return
            }
            
            guard let data = data,
                  let parsedModel = config.parser.parse(data: data)
            else {
                DispatchQueue.main.async {
                    completion(.failure(ParsingError.unknown))
                }
                return
            }
            DispatchQueue.main.async {
                completion(.success(parsedModel))
            }
        }
        
        task.resume()
    }
}
