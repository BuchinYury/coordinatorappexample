//
//  Presentable.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 07.06.2021.
//

import Foundation
import UIKit

protocol Presentable {
    
    func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
    
    func toPresent() -> UIViewController? {
        return self
    }
}
