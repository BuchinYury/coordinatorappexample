//
//  LoginCoordinator.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 10.06.2021.
//

import Foundation

protocol LoginModuleFactory {
    func makeLoginModule() -> (LoginModule, Presentable)
}

final class LoginModuleFactoryImpl: LoginModuleFactory {
    
    func makeLoginModule() -> (LoginModule, Presentable) {
        let presenter = LoginPresenterImpl()
        let interactor = LoginInteractorImpl(
            tokenStorage: AppAssembly.personalTokenService,
            presenter: presenter)
        let controller = LoginViewController(interactor: interactor)
        presenter.view = controller
        return (interactor, controller)
    }
}

protocol LoginCoordinatorOutput: class {
  var finishFlow: (() -> Void)? { get set }
}

final class LoginCoordinator: BaseCoordinator, LoginCoordinatorOutput {
    
    private let factory: LoginModuleFactory
    private let router: Router
    
    var finishFlow: (() -> Void)?
    
    init(factory: LoginModuleFactory,
         router: Router) {
        self.factory = factory
        self.router = router
    }
    
    override func start() {
        showLogin()
    }
    
    private func showLogin() {
        var (module, view) = factory.makeLoginModule()
        module.onFinish = { [weak self] in
            self?.finishFlow?()
        }
        router.setRootModule(view)
    }
}
