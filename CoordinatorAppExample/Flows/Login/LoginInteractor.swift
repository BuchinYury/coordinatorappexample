//
//  LoginInteractor.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation
import UIKit

protocol LoginInteractor: class {
    func onViewDidLoad()
    func onNextTapped(personalToken: String)
    func onWhatIsPersonalTokenButtonTapped()
}

final class LoginInteractorImpl: LoginInteractor, LoginModule {
    var onFinish: (() -> Void)?
    
    private let tokenStorage: PersonalTokenStorage
    private let presenter: LoginPresenter
    
    init(tokenStorage: PersonalTokenStorage,
         presenter: LoginPresenter) {
        self.tokenStorage = tokenStorage
        self.presenter = presenter
    }
    
    func onViewDidLoad() {
        presenter.present()
    }
    
    func onNextTapped(personalToken: String) {
        tokenStorage.save(token: Token(token: personalToken))
        onFinish?()
    }
    
    func onWhatIsPersonalTokenButtonTapped() {
        guard let url = URL(string: "https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token") else {
            return
        }
        
        UIApplication.shared.open(url)
    }
}
