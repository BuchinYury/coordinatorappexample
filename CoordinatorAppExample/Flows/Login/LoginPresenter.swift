//
//  LoginPresenter.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

protocol LoginPresenter: class {
    func present()
}

final class LoginPresenterImpl: LoginPresenter {
    
    weak var view: LoginView?
    
    func present() {
        #if DEBUG
        let token = "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"
        #else
        let token = nil
        #endif
        
        let model = LoginViewModel(token: token)
        
        view?.display(model)
    }
}

