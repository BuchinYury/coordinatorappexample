//
//  LoginViewController.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 10.06.2021.
//

import Foundation
import UIKit

protocol LoginModule: BaseModule {
    var onFinish: (() -> Void)? { get set }
}

struct LoginViewModel {
    let token: String?
}

protocol LoginView: class {
    func display(_ model: LoginViewModel)
}

final class LoginViewController: UIViewController {
    
   
    @IBOutlet weak var tokenTitleLable: UILabel!
    @IBOutlet weak var tokenLabel: UITextField!
    
    @IBAction func continueButton(_ sender: UIButton) {
        guard let token = tokenLabel.text else {
            return
        }
        
        interactor.onNextTapped(personalToken: token)
    }
    
    @IBAction func whatIsPersonalTokenButton(_ sender: Any) {
        interactor.onWhatIsPersonalTokenButtonTapped()
    }
    
    private let interactor: LoginInteractor
    
    init(interactor: LoginInteractor) {
        self.interactor = interactor
        super.init(nibName: "Login", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.onViewDidLoad()
    }
}

extension LoginViewController: LoginView {
    func display(_ model: LoginViewModel) {
        tokenLabel.text = model.token
    }
}
