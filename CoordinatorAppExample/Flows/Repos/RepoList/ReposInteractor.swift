//
//  ItemsInteractor.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 10.06.2021.
//

import Foundation

protocol ReposModule: BaseModule {
    var onItemSelect: ((RepoViewModel) -> ())? { get set }
    var onExit: (() -> ())? { get set }
}

protocol ReposInteractor: class {
    func onViewDidLoad()
    func onSelect(item: RepoViewModel)
    func onExitTapped()
}

final class ReposInteractorImpl: ReposInteractor, ReposModule {
    
    var onItemSelect: ((RepoViewModel) -> ())?
    var onExit: (() -> ())?
    
    private let presenter: ReposPresenter
    private let reposService: ReposService
    
    init(presenter: ReposPresenter,
         reposService: ReposService) {
        self.presenter = presenter
        self.reposService = reposService
    }
    
    func onViewDidLoad() {
        reposService.loadRepos { [weak self] result in
            switch result {
            case .success(let repos):
                self?.presenter.present(repos)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func onSelect(item: RepoViewModel) {
        onItemSelect?(item)
    }
    
    func onExitTapped() {
        onExit?()
    }
}
