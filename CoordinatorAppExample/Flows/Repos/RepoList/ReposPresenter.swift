//
//  ItemsPresenter.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 10.06.2021.
//

import Foundation

protocol ReposPresenter: class {
    func present(_ repos: [Repository])
}

final class ReposPresenterImpl: ReposPresenter {
    
    weak var view: ReposView?
    
    func present(_ repos: [Repository]) {
        let items = repos.map { repository in
            RepoViewModel(title: repository.fullName, subtitle: repository.description ?? "")
        }
        
        view?.display(items: items)
    }
}
