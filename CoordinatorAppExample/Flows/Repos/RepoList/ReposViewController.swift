//
//  ViewController.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 07.06.2021.
//

import UIKit

struct RepoViewModel {
    let title: String
    let subtitle: String
}

protocol ReposView: class {
    func display(items: [RepoViewModel])
}

final class ReposViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var items: [RepoViewModel] = []
    
    private let interactor: ReposInteractor
    
    init(interactor: ReposInteractor) {
        self.interactor = interactor
        super.init(nibName: "Repos", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Items"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Выйти",
            style: .plain,
            target: self,
            action: #selector(exitTapped))
        
        interactor.onViewDidLoad()
    }
    
    @objc func exitTapped() {
        interactor.onExitTapped()
    }
}

extension ReposViewController: ReposView {
    
    func display(items: [RepoViewModel]) {
        self.items = items
        tableView.reloadData()
    }
}

extension ReposViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.onSelect(item: items[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ReposViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = item.title
        cell.detailTextLabel?.text = item.subtitle
        return cell
    }
}
