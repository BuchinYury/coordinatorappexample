//
//  ItemsCoordinator.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 09.06.2021.
//

import Foundation
import UIKit

protocol ItemsModuleFactory {
    func makeItemsModule() -> (ReposModule, Presentable)
    func makeSelectedRepo(item: RepoViewModel) -> (SelectedRepoModule, Presentable)
}

final class ItemsModuleFactoryImpl: ItemsModuleFactory {
        
    func makeItemsModule() -> (ReposModule, Presentable) {
        let reposService = AppAssembly.reposServiceImpl
        let presenter = ReposPresenterImpl()
        let interactor = ReposInteractorImpl(presenter: presenter, reposService: reposService)
        let controller = ReposViewController(interactor: interactor)
        presenter.view = controller
        return (interactor, controller)
    }
    
    func makeSelectedRepo(item: RepoViewModel) -> (SelectedRepoModule, Presentable) {
        let presenter = SelectedRepoPresenterImpl()
        let interactor = SelectedRepoInteractorImpl(item: item, presenter: presenter)
        let controller = SelectedRepoViewController(interactor: interactor)
        presenter.view = controller
        return (interactor, controller)
    }
}

protocol ItemsCoordinatorOutput: class {
  var finishFlow: (() -> Void)? { get set }
}

final class ItemsCoordinator: BaseCoordinator, ItemsCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let router: Router
    private let factory: ItemsModuleFactory
    private let personalTokenStorage: PersonalTokenStorage
    
    init(router: Router,
         factory: ItemsModuleFactory,
         personalTokenStorage: PersonalTokenStorage) {
        self.router = router
        self.factory = factory
        self.personalTokenStorage = personalTokenStorage
    }
    
    override func start() {
        showItemList()
    }
    
    //MARK: - Run current flow's controllers
    
    private func showItemList() {
        var (module, view) = factory.makeItemsModule()
        module.onItemSelect = { [weak self] item in
            self?.showItemDetail(item)
        }
        module.onExit = { [weak self] in
            self?.personalTokenStorage.dropToken()
            self?.finishFlow?()
        }
        router.setRootModule(view)
    }
    
    private func showItemDetail(_ item: RepoViewModel) {
        
        let (_, view) = factory.makeSelectedRepo(item: item)
        router.push(view, hideBottomBar: true)
    }
}
