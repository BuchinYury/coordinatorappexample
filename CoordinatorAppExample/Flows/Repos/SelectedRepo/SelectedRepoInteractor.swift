//
//  SelectedRepoInteractor.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

protocol SelectedRepoModule: BaseModule {
    
}

protocol SelectedRepoInteractor: class {
    func onViewDidLoad()
}

final class SelectedRepoInteractorImpl: SelectedRepoInteractor, SelectedRepoModule {
    
    private let item: RepoViewModel
    private let presenter: SelectedRepoPresenter
    
    init(item: RepoViewModel,
         presenter: SelectedRepoPresenter) {
        self.item = item
        self.presenter = presenter
    }
    
    func onViewDidLoad() {
        presenter.present(item)
    }
}

