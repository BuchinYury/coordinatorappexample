//
//  SelectedRepoPresenter.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

protocol SelectedRepoPresenter: class {
    func present(_ repo: RepoViewModel)
}

final class SelectedRepoPresenterImpl: SelectedRepoPresenter {
    
    weak var view: SelectedRepoView?
    
    func present(_ repo: RepoViewModel) {
        view?.display(item: repo)
    }
}
