//
//  SelectedRepoViewController.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation
import UIKit

protocol SelectedRepoView: class {
    func display(item: RepoViewModel)
}

final class SelectedRepoViewController: UIViewController {
    
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var repoDescriptionLabel: UILabel!
    
    private let interactor: SelectedRepoInteractor
    
    init(interactor: SelectedRepoInteractor) {
        self.interactor = interactor
        super.init(nibName: "SelectedRepo", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor.onViewDidLoad()
    }
}

extension SelectedRepoViewController: SelectedRepoView {
    func display(item: RepoViewModel) {
        repoNameLabel.text = item.title
        repoDescriptionLabel.text = item.subtitle
    }
}
