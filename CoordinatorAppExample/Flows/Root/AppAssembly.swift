//
//  AppAssembly.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

// TODO drop static
enum AppAssembly {
    
    private static let keychainStorage = KeychainStorage()
    private static let userDefaultsStorage = UserDefaultsStorage()
    private static let networkClient = NetworkClient()
    
    static let personalTokenService = PersonalTokenService(storage: keychainStorage)
    static let reposServiceImpl = ReposServiceImpl(
        networkClient: networkClient,
        personalTokenProvider: personalTokenService)
}
