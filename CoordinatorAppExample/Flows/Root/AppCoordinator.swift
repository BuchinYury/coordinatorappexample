//
//  AppCoordinator.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 07.06.2021.
//

final class AppCoordinator: BaseCoordinator {
    
    private let coordinatorFactory: CoordinatorsFactory
    private let router: Router
    private let tokenProvider: PersonalTokenProvider
    
    init(router: Router,
         coordinatorFactory: CoordinatorsFactory,
         tokenProvider: PersonalTokenProvider) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
        self.tokenProvider = tokenProvider
    }
    
    override func start() {
        tokenProvider.token() == nil
            ? runLoginFlow()
            : runItemsFlow()
    }
}

private extension AppCoordinator {
    
    func runLoginFlow() {
        guard let navController = router.rootController
        else { return }
        
        let coordinator = coordinatorFactory.makeLoginCoordinator(navController: navController)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.runItemsFlow()
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    func runItemsFlow() {
        guard let navController = router.rootController
        else { return }
        
        let coordinator = coordinatorFactory.makeItemsCoordinator(navController: navController)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.runLoginFlow()
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start()
    }
}
