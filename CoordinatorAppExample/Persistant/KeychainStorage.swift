//
//  KeychainStorage.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation
import Security

final class KeychainStorage {
    
    private let jsonEncoder = JSONEncoder()
    private let jsonDecoder = JSONDecoder()
}

extension KeychainStorage: Storage {
    func object<ObjectType: DataStorable>(byIdentifier identifier: String) -> ObjectType? {
        var query = KeychainStoreItemOptions().query()
        query[String(kSecAttrAccount)] = identifier
        query[String(kSecMatchLimit)] = kSecMatchLimitOne
        query[String(kSecReturnData)] = kCFBooleanTrue
        
        var result: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &result)
        
        guard status == errSecSuccess, let data = result as? Data else {
            printOSStatus(status, "\(ObjectType.self): \(identifier)")
            return nil
        }
        
        return ObjectType.decodeObject(from: data, decoder: self.jsonDecoder)
    }
    
    func set<ObjectType: DataStorable>(object: ObjectType) {
        remove(object: object)
        
        let data = object.data(encoder: self.jsonEncoder)
        
        var query = KeychainStoreItemOptions().query()
        query[String(kSecAttrAccount)] = object.storeIdentifier
        query[String(kSecValueData)] = data
        
        let status = SecItemAdd(query as CFDictionary, nil)
        printOSStatus(status, "\(ObjectType.self): \(object.storeIdentifier)")
    }
    
    func remove<ObjectType: DataStorable>(object: ObjectType) {
        remove(byIdentifier: object.storeIdentifier)
    }
    
    func remove(byIdentifier identifier: String) {
        var query = KeychainStoreItemOptions().query()
        query[String(kSecAttrAccount)] = identifier
        
        let status = SecItemDelete(query as CFDictionary)
        if status != errSecSuccess && status != errSecItemNotFound {
            printOSStatus(status, "\(identifier)")
        }
    }
}

private extension KeychainStorage {
    func printOSStatus(_ status: OSStatus, _ info: String = "", _ function: String = #function) {
        #if DEBUG
        if status != errSecSuccess {
            print("\(function); code: \(status), \(SecCopyErrorMessageString(status, nil) as Any), \(info)")
        }
        #endif
    }

    struct KeychainStoreItemOptions {
        
        func query() -> [String: Any] {
            return [
                String(kSecClass): kSecClassGenericPassword,
                String(kSecAttrAccessible): kSecAttrAccessibleAfterFirstUnlock,
                String(kSecAttrService): KeychainStorage.keychainZone
            ]
        }
    }
}

extension KeychainStorage {
    static let keychainZone = "CoordinatorAppExample"
}

