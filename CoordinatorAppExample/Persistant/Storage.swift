//
//  Storage.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

protocol Storable {
    var storeIdentifier: String { get }
}

protocol DataStorable: Storable {
    func data(encoder: JSONEncoder) -> Data?
    static func decodeObject<ObjectType: Storable>(from data: Data, decoder: JSONDecoder) -> ObjectType?
}

// MARK: - Default Implementations

extension DataStorable where Self: Codable {
    func data(encoder: JSONEncoder) -> Data? {
        return try? encoder.encode(self)
    }

    static func decodeObject<ObjectType>(from data: Data, decoder: JSONDecoder) -> ObjectType? {
        guard let decodedObject = try? decoder.decode(Self.self, from: data) else {
            return nil
        }
        
        return decodedObject as? ObjectType
    }
}

protocol Storage {
    func object<ObjectType: DataStorable>(byIdentifier identifier: String) -> ObjectType?
    func set<ObjectType: DataStorable>(object: ObjectType)
    func remove<ObjectType: DataStorable>(object: ObjectType)
    func remove(byIdentifier identifier: String)
}
