//
//  KeychainStorage.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation
import Security

final class UserDefaultsStorage {
    
    private let jsonEncoder = JSONEncoder()
    private let jsonDecoder = JSONDecoder()
    private let userDefaults = UserDefaults.standard
}

// MARK: - IStorage

extension UserDefaultsStorage: Storage {
    func object<ObjectType: DataStorable>(byIdentifier identifier: String) -> ObjectType? {
        guard let data = userDefaults.data(forKey: identifier)
        else { return nil }
        
        return ObjectType.decodeObject(from: data, decoder: self.jsonDecoder)
    }
    
    func set<ObjectType: DataStorable>(object: ObjectType) {
        remove(object: object)
        
        userDefaults.set(
            object.data(encoder: jsonEncoder),
            forKey: object.storeIdentifier)
    }
    
    func remove<ObjectType: DataStorable>(object: ObjectType) {
        remove(byIdentifier: object.storeIdentifier)
    }
    
    func remove(byIdentifier identifier: String) {
        userDefaults.removeObject(forKey: identifier)
    }
}
