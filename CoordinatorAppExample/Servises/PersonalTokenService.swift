//
//  PersonalTokenProvider.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

protocol PersonalTokenProvider {
    func token() -> Token?
}

// TODO bad naming
protocol PersonalTokenStorage {
    func save(token: Token)
    func dropToken()
}

final class PersonalTokenService {
    
    private let storage: Storage
    
    init(storage: Storage) {
        self.storage = storage
    }
}

extension PersonalTokenService: PersonalTokenProvider {
    func token() -> Token? {
        storage.object(byIdentifier: Token.identifier)
    }
}

extension PersonalTokenService: PersonalTokenStorage {
    func save(token: Token) {
        storage.set(object: token)
    }
    
    func dropToken() {
        storage.remove(byIdentifier: Token.identifier)
    }
}

struct Token: Codable {
    let token: String
}

extension Token: DataStorable {
    
    static let identifier = "Token"
    
    var storeIdentifier: String {
        Token.identifier
    }
}
