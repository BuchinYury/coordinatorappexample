//
//  ReposService.swift
//  CoordinatorAppExample
//
//  Created by y.buchin on 11.06.2021.
//

import Foundation

protocol ReposService {
    func loadRepos(completion: @escaping (Result<[Repository], Error>) -> Void)
}

enum ReposServiceError: Error {
    case badToken
}

final class ReposServiceImpl: ReposService {
    
    private let networkClient: NetworkClient
    private let personalTokenProvider: PersonalTokenProvider
    
    init(networkClient: NetworkClient,
         personalTokenProvider: PersonalTokenProvider) {
        self.networkClient = networkClient
        self.personalTokenProvider = personalTokenProvider
    }
    
    func loadRepos(completion: @escaping (Result<[Repository], Error>) -> Void) {
        guard let personalToken = personalTokenProvider.token()?.token else {
            completion(.failure(ReposServiceError.badToken))
            return
        }
        
        networkClient.send(
            config: .init(
                request: ReposRequest(token: personalToken),
                parser: ReposParser()),
            completion: completion)
    }
}

struct ReposRequest: Request {
    
    let urlRequest: URLRequest?
    
    init(token: String) {
        guard let url = URL(string: "https://api.github.com/user/repos") else {
            self.urlRequest = nil
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("token \(token)", forHTTPHeaderField: "Authorization")
        
        self.urlRequest = urlRequest
    }
}

struct ReposParser: Parser {
    
    func parse(data: Data) -> [Repository]? {
        do {
            return try JSONDecoder().decode([Repository].self, from: data)
        } catch {
            return nil
        }
    }
}

struct Repository: Codable {
    
    let fullName: String
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case fullName = "full_name"
        case description
    }
}
